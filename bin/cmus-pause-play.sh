#!/bin/bash
#
# Script for conditionally pausing or playing cmus

cmus-remote -Q 2>/dev/null 1>&2
REMOTE=$?
if [ $REMOTE != "0" ]; then
	exit 0;
fi

STATUS=$(cmus-remote -Q | grep status | cut -d \  -f 2)
if [ $STATUS = "playing" ]; then
	cmus-remote --pause 2>/dev/null || true
else
	cmus-remote --play 2>/dev/null || true
fi
	
