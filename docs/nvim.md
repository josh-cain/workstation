# NeoVim/Vim Commands You Should Remember

There are tons of good guides and tutorials out there, but this is the small sub-set of commands and tips I find most useful.

## Basics

`=` is the key to format lines.  However, it should be followed by the # of lines you wish to format, then `Enter`.  For instance, `=` + `5` + `Enter` formats the next five lines.  `gg` + `=` + `G` is a quick key sequence to skip to the top and format the file down to the bottom.
