# CMUS usage
Basic usage/recipes for CMUS music player.

## Basic Navigation + Playback
`1`, `2`, `3`, `4`, `5` select views
`tab` toggles between windows in a selected view

`x` play track
`c` pause playback
`v` stop playback

`b` next track
`z` previous track
`l` fast forward (5s)
`h` rewind (5s)

`C` toggle continue
`r` toggle repeat
`s` toggle shuffle
`<ctrl> + R` toggle repeat current item

`-` volume down
`=` volume up (+ key)

## Setup
`:add $DIR` add music to library
`D` delete track(s) from library (doesn't remove files)

## Playback and Audio
`?` play track
`h` rewind

## Marking Tracks/Playlists
`<space>` marks/unmarks tracks from the varous views
`:unmark` unmark all tracks

`y` copy marked tracks to the selected playlist
`e` append marked tracks to the play queue
`E` prepend marked tracks to the play queue

** can only mark tracks in views 2-4

## Playlist Management
`pl-create $NAME` create a new playlist
`pl-rename $NAME` 
