# Josh's Workstation
### Basic CM for my Personal Environment

Note that this repository represents my personal environment.  It is not broadly tested - in fact, I run the ansible playbooks included here exclusively against MacOSX (ugh).

## Manual Steps
In order to get to where we can run basic playbooks, some manual workstation customization is required.

### Install Software
 - Firefox (from website)
 - iTerm2 (from website)
 - XCode developer tools (`xcode-select --install`)
 - Alfred (from website)
 - Homebrew (instructions on site)
 - Ansible (`brew install ansible`)

### Non-versioned preferences
 - scroll direction
 - screensaver
 - profile icon

### Snag SSH Key(s)

<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
