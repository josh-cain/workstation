# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

export GPG_TTY='tty'
export HISTCONTROL='ignorespace'
export ANSIBLE_NOCOWS=1

# For Proxy
#export HTTP_PROXY='localhost:8080'
#export HTTPS_PROXY='localhost:8080'

export REQUESTS_CA_BUNDLE=/etc/pki/tls/certs/ca-bundle.trust.crt

alias gpg=gpg2
alias pkcs12='openssl pkcs12'
alias vi=vim
alias code='/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=code --file-forwarding com.visualstudio.code --new-window'
alias xclip="xclip -selection c"

set -o vi

if [ -f `which powerline-daemon` ]; then
	powerline-daemon -q
	POWERLINE_BASH_CONTINUATION=1
        POWERLINE_BASH_SELECT=1
        . /usr/share/powerline/bash/powerline.sh
fi

# Initalize sdkman if it exists
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

ulimit -Sn 4096
