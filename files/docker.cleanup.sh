#! /bin/bash

# Remove all containers not used in the last 2 weeks
docker container prune -f --filter "until=336h"
# Remove images not associated with a container
docker image prune -af
# Remove networks not in use
docker network prune -f
# Remove volumes not in use
docker volume prune -f
# Remove build cache not in use
docker builder prune -f
