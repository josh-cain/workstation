# Personal customizations

# vim mode
bindkey -v

# tweak a few commands
alias vi=nvim
alias vim=nvim
alias grep=ggrep
alias sed=gsed
alias k=kubectl
alias ns='kubectl config set-context --current --namespace'
alias hg='history | rg'
alias ij='nohup idea . &>/dev/null &'

# environment vars
export EDITOR=nvim
export ANSIBLE_NOCOWS=1

# key bindings
bindkey "^j" history-search-forward
bindkey "^k" history-search-backward

# let's make linting recent change easier
alias lintchanged='git diff --name-only HEAD~1 | rg lib | xargs eslint'

# not intentional
alias pp='pino-pretty -c -l -S'

# Make switching jdk versions as easy as "jdk 12"
# From https://github.com/AdoptOpenJDK/homebrew-openjdk
jdk() {
  version=$1
  export JAVA_HOME=$(/usr/libexec/java_home -v"$version");
  java -version
}

# because Mac... I miss linux.
export GPG_TTY=$(tty)

# Include postgres in Path
export PATH="/usr/local/opt/openjdk@11/bin:$PATH:/Applications/IntelliJ IDEA.app/Contents/MacOS/:$HOME/bin"

# Node Version Manager (NVM), see https://github.com/nvm-sh/nvm
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# We need Homebrew stuffs in env
eval "$(/opt/homebrew/bin/brew shellenv)"
