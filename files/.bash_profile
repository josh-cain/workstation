# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

#Add redhat-git-utils since the update script relies on them being on the path
PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

