set nocompatible              " be iMproved, required
filetype on

" Vanilla customizations
set hidden
" https://jeffkreeftmeijer.com/vim-number/
set number relativenumber
" Because Apple thinks real escape keys are stupid :(
nnoremap <Space> i_<Esc>r

call plug#begin('~/.vim/plugged')
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'scrooloose/nerdtree'
Plug 'qpkorr/vim-bufkill'
Plug 'mhinz/vim-signify'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'w0rp/ale'
Plug 'pangloss/vim-javascript'
Plug 'ap/vim-buftabline'

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()

" Plugin use
let NERDTreeQuitOnOpen=1
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
"
" Ctrl + N opens nerdtree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" Buffer Switching
nnoremap <C-l> :bnext<CR>
nnoremap <C-h> :bprev<CR>

" close tabs
nnoremap <C-k> :bd<CR>
" save all
nnoremap <C-s> :wa<CR>

" soft tabs.  for the record, I prefer hard tabs, but am tired of creating
" whitespace changes and autho-detection violates KISS
set expandtab
set shiftwidth=2
set softtabstop=2

" Color Schemes
set termguicolors
colorscheme onehalfdark

" Customize ctrl+P
if executable('rg')
  set grepprg=rg\ --color=never
  let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'
  let g:ctrlp_use_caching = 0
endif

" folding
set foldmethod=syntax
let javaScript_fold=1
set foldlevelstart=99

" configure ALE for linting
let g:ale_sign_column_always = 1

" configure signify to show VCS status
highlight SignColumn ctermbg=NONE cterm=NONE guibg=NONE gui=NONE
let g:signify_vcs_list              = [ 'git' ]
let g:signify_cursorhold_insert     = 1
let g:signify_cursorhold_normal     = 1
let g:signify_update_on_bufenter    = 0
let g:signify_update_on_focusgained = 1

" configure sessions
" save session only if vcs root is detected (.git)
" https://stackoverflow.com/questions/5142099/how-to-auto-save-vim-session-on-quit-and-auto-reload-on-start-including-split-wi
fu! SaveSess()
    if isdirectory(getcwd() . '/.git')
        execute 'mksession! ' . getcwd() . '/.session.vim'
    endif
endfunction

fu! RestoreSess()
if filereadable(getcwd() . '/.session.vim')
    execute 'so ' . getcwd() . '/.session.vim'
    if bufexists(1)
        for l in range(1, bufnr('$'))
            if bufwinnr(l) == -1
                exec 'sbuffer ' . l
            endif
        endfor
    endif
endif
endfunction

" autocmd VimLeave * call SaveSess()
" autocmd VimEnter * nested call RestoreSess()
